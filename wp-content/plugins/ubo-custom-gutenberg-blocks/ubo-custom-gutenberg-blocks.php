<?php
/*
Plugin Name: UBO - Custom Gutenberg Blocks
Description: Plugin de customisation des blokcs Gutenberg pour UBO.
Version: 1.0
Author: Rémi ZAMMUT
Text Domain: ubo-custom-gutenberg-blokcs

*/

/* Init JS */
function ubo_guten_custom_js() {
    wp_enqueue_script( 'ubo-guten-blocks-custom-script',
        plugins_url( 'js/ubo-custom-blocks.js', __FILE__ ),
        array( 'wp-blocks' )
    );
}
add_action( 'enqueue_block_editor_assets', 'ubo_guten_custom_js' );

/* Init CSS */
function ubo_guten_custom_css() {
    wp_enqueue_style( 'ubo-guten-blocks-custom-style', 
    	plugins_url( 'css/ubo-custom-blocks.css', __FILE__ ) );
}
add_action( 'enqueue_block_assets', 'ubo_guten_custom_css' );