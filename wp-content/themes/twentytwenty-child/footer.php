<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>
			<footer id="site-footer" role="contentinfo" class="header-footer-group">

				<div class="section-inner">

					<div class="footer-credits">

						<p class="footer-copyright">&copy;
							<?php
							echo date_i18n(
								/* translators: Copyright date format, see https://secure.php.net/date */
								_x( 'Y', 'copyright date format', 'twentytwenty' )
							);
							?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
						</p><!-- .footer-copyright -->

						<!--<p class="powered-by-wordpress">
							<a href="<?php //echo esc_url( __( 'https://wordpress.org/', 'twentytwenty' ) ); ?>">
								<?php //_e( 'Powered by WordPress', 'twentytwenty' ); ?>
							</a>
						</p>--><!-- .powered-by-wordpress -->

					</div><!-- .footer-credits -->

					<?php
					// Récupération du logo définit pour le footer ou application du logo par défaut
					$footer_logo = get_theme_mod( 'footer_logo' );
					if(empty($footer_logo)){
						$footer_logo = get_theme_file_uri() . '/assets/images/logo-ubo-ultra-bio-ozone-white.svg';
					}
					?>
					<div class="site-logo faux-heading">
						<a href="<?php echo get_home_url() ?>" class="footer-logo-link" rel="home">
							<img src="<?php echo $footer_logo ?>" class="footer-logo" alt="<?php echo get_bloginfo( 'name' ) ?>">
						</a>
							<span class="screen-reader-text"><?php echo get_bloginfo( 'name' ) ?></span>
					</div>
					

					
					<?php //twentytwenty_site_logo(); ?>

					<a class="to-the-top" href="#site-header">
						<span class="to-the-top-long">
							<?php
							/* translators: %s: HTML character for up arrow */
							printf( __( 'To the top %s', 'twentytwenty' ), '<span class="arrow" aria-hidden="true">&uarr;</span>' );
							?>
						</span><!-- .to-the-top-long -->
						<span class="to-the-top-short">
							<?php
							/* translators: %s: HTML character for up arrow */
							printf( __( 'Up %s', 'twentytwenty' ), '<span class="arrow" aria-hidden="true">&uarr;</span>' );
							?>
						</span><!-- .to-the-top-short -->
					</a><!-- .to-the-top -->

				</div><!-- .section-inner -->

			</footer><!-- #site-footer -->

		<?php wp_footer(); ?>

	</body>
</html>
