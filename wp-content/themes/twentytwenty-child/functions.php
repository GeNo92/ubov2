<?php
function wpm_enqueue_styles(){
wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'wpm_enqueue_styles' );

/*
 *  Ajout du JS et CSS dans le wp_head
 */
add_action( "wp_enqueue_scripts", "include_scripts" );

function include_scripts()
{
    wp_enqueue_style("stylesheet-style", get_stylesheet_directory_uri() . "/style.css");

    // Ajout CSS du thème
    wp_enqueue_style("stylesheet-ubo-custom", get_stylesheet_directory_uri() . "/assets/css/ubo-custom-style.css");

    // Ajout CCS de l'éditeur
    wp_enqueue_style("stylesheet-style-parent", get_stylesheet_directory_uri() . "/assets/css/editor-style-classic.css");
}

/* 
 * Allow upload of SVG image
 */
function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');

/**
 * Enqueue supplemental block editor styles.
 */
function twentytwenty_child_block_editor_styles() {

    $css_dependencies = array();

    // Enqueue the editor styles.
    wp_enqueue_style( 'twentytwenty-block-editor-styles', get_theme_file_uri( '/assets/css/editor-style-block.css' ), $css_dependencies, wp_get_theme()->get( 'Version' ), 'all' );
    wp_style_add_data( 'twentytwenty-block-editor-styles', 'rtl', 'replace' );

    // Add inline style from the Customizer.
    wp_add_inline_style( 'twentytwenty-block-editor-styles', twentytwenty_get_customizer_css( 'block-editor' ) );

    // Add inline style for non-latin fonts.
    wp_add_inline_style( 'twentytwenty-block-editor-styles', TwentyTwenty_Non_Latin_Languages::get_non_latin_css( 'block-editor' ) );

    // Enqueue the editor script.
    wp_enqueue_script( 'twentytwenty-block-editor-script', get_theme_file_uri( '/assets/js/editor-script-block.js' ), array( 'wp-blocks', 'wp-dom' ), wp_get_theme()->get( 'Version' ), true );
}

add_action( 'enqueue_block_editor_assets', 'twentytwenty_child_block_editor_styles', 1, 1 );


/*
 * Add Footer section for logo
 */
add_action('customize_register', 'theme_footer_customizer', 99);
function theme_footer_customizer($wp_customize){
    //adding section in wordpress customizer   
    $wp_customize->add_section('footer_settings_section', array(
      'title'          => 'Footer options'
    ));
    //adding setting for footer logo
    $wp_customize->add_setting('footer_logo');
    $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize,'footer_logo',array(
     'label'      => 'Footer Logo',
     'section'    => 'footer_settings_section',
     'settings'   => 'footer_logo',
     ))); 

    // Remove customizer sections/panels parent theme
    // Site identity
    $wp_customize->remove_control('blogname');
    $wp_customize->remove_control('blogdescription');
    $wp_customize->remove_control('retina_logo');
    
    /* Colors */
    $wp_customize->remove_setting('accent_hue_active');
    $wp_customize->remove_control('accent_hue_active');
    $wp_customize->remove_control('accent_hue');

    /*$wp_customize->remove_control('enable_header_search');
    $wp_customize->remove_control('show_author_bio'); */
    
    $wp_customize->remove_section('background_section');
    $wp_customize->remove_section('background_image');
    $wp_customize->remove_section('cover_template_options');
    $wp_customize->remove_panel('widgets');
}


/**
 * Logo & Description custom
 */
/**
 * Displays the site logo or the default logo
 *
 * @param array   $args Arguments for displaying the site logo either as an image or text.
 * @param boolean $echo Echo or return the HTML.
 *
 * @return string $html Compiled HTML based on our arguments.
 */
function twentytwenty_child_site_logo( $args = array(), $echo = true ) {
    $logo       = get_custom_logo();
    if(empty($logo)){
        $custom_logo = get_theme_file_uri() . '/assets/images/logo-ubo-ultra-bio-ozone.svg';
        $logo = '<a href="'. get_home_url() .'" class="custom-logo-link" rel="home"><img src="'. $custom_logo .'" class="custom-logo" alt="'. get_bloginfo( 'name' ) .'"></a>';
    }
    //var_dump($logo); die;
    $site_title = get_bloginfo( 'name' );
    $contents   = '';
    $classname  = '';

    $defaults = array(
        'logo'        => '%1$s<span class="screen-reader-text">%2$s</span>',
        'logo_class'  => 'site-logo',
        'title'       => '<a href="%1$s">%2$s</a>',
        'title_class' => 'site-title',
        'home_wrap'   => '<h1 class="%1$s">%2$s</h1>',
        'single_wrap' => '<div class="%1$s faux-heading">%2$s</div>',
        'condition'   => ( is_front_page() || is_home() ) && ! is_page(),
    );

    $args = wp_parse_args( $args, $defaults );

    /**
     * Filters the arguments for `twentytwenty_site_logo()`.
     *
     * @param array  $args     Parsed arguments.
     * @param array  $defaults Function's default arguments.
     */
    $args = apply_filters( 'twentytwenty_site_logo_args', $args, $defaults );

    //if ( has_custom_logo() ) {
        $contents  = sprintf( $args['logo'], $logo, esc_html( $site_title ) );
        $classname = $args['logo_class'];
    //} else {
      //  $contents  = sprintf( $args['title'], esc_url( get_home_url( null, '/' ) ), esc_html( $site_title ) );
       // $classname = $args['title_class'];
    //}

    $wrap = $args['condition'] ? 'home_wrap' : 'single_wrap';

    $html = sprintf( $args[ $wrap ], $classname, $contents );

    /**
     * Filters the arguments for `twentytwenty_site_logo()`.
     *
     * @param string $html      Compiled html based on our arguments.
     * @param array  $args      Parsed arguments.
     * @param string $classname Class name based on current view, home or single.
     * @param string $contents  HTML for site title or logo.
     */
    $html = apply_filters( 'twentytwenty_site_logo', $html, $args, $classname, $contents );

    if ( ! $echo ) {
        return $html;
    }

    echo $html; //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

}

